# Инструкция  
### Что это?  
Это небольшая рабочая среда для сборки модулей для [psy.su.](https://psy.su/) К сожалению, проджект не самый маленький, контроля версий у нас нет, да и вообще... Так что поэтому пока что предлагаю добавлять новые элементы и рефакторить старые таким вот образом.

### Каким образом?
В файле [index.pug](dev/index.pug) содержится код, импортирующий модули, и задающий атрибуты каждого из них. Например, вот как это выглядит для модуля AdblockHostedEvent

```
- // Define appearance of adblock
- var colorTheme = 'colortheme-pale-yellow';
- // Position adblock
- var floatClass = 'float-left';
- // Create data for adblock
- var dataLink = 'https://apple.com';
- var eventCategory = 'Обучение';
- var hostPictureUrl = 'http://www.thefandom.net/wp-content/uploads/2017/08/milla-jovovich-sq.jpg'
- var hostName = 'Петрова Елена Юрьевна';
- var eventDate = '27 октября 2018 – 05 ноября 2018';
- var eventType = 'Авторская программа';
- var eventTitle = 'Клинические проблемы в контексте психологического консультирования';
- var eventPrice = '18 800 руб.';
- var eventActionText = 'Посмотреть';
include AdblockHostedEvent/AdblockHostedEvent.pug
```
Остается изменить параметры на нужные, зайти терминалом в директорию, где лежит readme, который вы сейчас читаете, сказать

```
npm start
```
и дать компьютеру подумать пару секунд. Всё, готовый модуль можно копипастить из [www/index.html.](www/index.html)

### И что дальше?  
Копируем полученный код модуля в нужное место шаблона и подменяем style.css на сайте тем, который лежит в директории этого проекта.

*Цветовая схема пока что одна, остальные будем дописывать по мере надобности.<br>

**В код модуля залезть [можно,](dev/AdblockHostedEvent/AdblockHostedEvent.pug) но не нужно, локальные заплатки имеют свойство накапливаться 😊

***Перед началом работы не забудьте сделать
```
npm i
```
